Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: konversation
Upstream-Contact: kde-devel@kde.org
Source: https://invent.kde.org/network/konversation
Comment:This package was debianized by Nathaniel W. Turner <nate@houseofnate.net> on
 Fri, 10 Oct 2003 23:39:24 -0400.
 Maintenance was taken over by Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org> on
 Tue, 17 Jul 2007 03:09:05 +0300

Files: *
Copyright: 2003, Alex Zepeda <zipzippy@sonic.net>
           2003, Benjamin C Meyer (ben+kdelibs at meyerhome dot net)
           2009-2011, Bernd Buschinski <b.buschinski@web.de>
           2002, Carsten Pfeiffer <pfeiffer@kde.org>
           2003-2004, Christian Muehlhaeuser <chris@chris.de>
           2002-2006, Dario Abatianni <eisfuchs@tigress.com>
           2005-2014, Eike Hein <hein@kde.org>
           2004-2011, Eli Mackenzie <argonel@gmail.com>
           2020, Friedrich W. H. Kossebau <kossebau@kde.org>
           2004, Gary Cramblitt <garycramblitt@comcast.net>
           2005, Gábor Lehel <illissius@gmail.com>
           2005, Ismail Donmez <ismail@kde.org>
           2005, Ivor Hewitt <ivor@ivor.org>
           2004-2005, John Tapsell <john@geola.co.uk>
           2005-2006, John Tapsell <johnflux@gmail.com>
           2005-2007, Joris Guisson <joris.guisson@gmail.com>
           2003, Laur Ivan <laurivan@eircom.net>
           2008, Mark Kretschmann <kretschmann@kde.org>
           2010, Martin Blumenstingl <darklight.xdarklight@googlemail.com>
           1999, Martin R. Jones <mjones@kde.org>
           2002, Matthias Gierlings <gismore@users.sourceforge.net>
           2004-2005, Max Howell <max.howell@methylblue.com>
           2004, Michael Brade <brade@kde.org>
           2004, Michael Goettsche <mail@tuxipuxi.de>
           2006-2009, Michael Kreitzer <mrgrim@gr1m.org>
           2003, Mickael Marchand <marchand@kde.org>
           1998, Netscape Communications Corporation
           2004-2017, Peter Simonsson <peter.simonsson@gmail.com>
           2003-2007, Peter Simonsson <psn@linux.se>
           2005, Renchi Raju <renchi@pooh.tam.uiuc.edu>
           1997, Robey Pointer <robeypointer@gmail.com>
           2004-2006, Seb Ruiz <ruiz@kde.org>
           2004-2008, Shintaro Matsuoka <shin@shoegazed.org>
           2007, Terence Simpson
           2009, Travis McHenry <tmchenryaz@cox.net>
           2009, Travis McHenry <wordsizzle@gmail.com>
           2003, Waldo Bastian <bastian@kde.org>
           2004, İsmail Dönmez <ismail.donmez@boun.edu.tr>
           2005, İsmail Dönmez <ismail@kde.org.tr>
           2004-2005, İsmail Dönmez <ismail@kde.org>
License: GPL-2+

Files: data/org.kde.konversation.appdata.xml
Copyright: 2016, Burkhard Lück <lueck@hube-lueck.de>
License: CC0-1.0

Files: data/scripting_support/python/konversation/dbus.py
       data/scripting_support/python/konversation/i18n.py
       data/scripts/bug
       data/scripts/cmd
       data/scripts/media
       data/scripts/sayclip
       data/scripts/sysinfo
       src/config/connectionbehavior_config.cpp
       src/config/connectionbehavior_config.h
       src/irc/topichistorymodel.cpp
       src/irc/topichistorymodel.h
       src/irc/outputfilterresolvejob.cpp
       src/irc/invitedialog.h
       src/irc/ircqueue.cpp
       src/irc/outputfilterresolvejob.h
       src/irc/invitedialog.cpp
       src/irc/ircqueue.h
       src/queuetuner.cpp
       src/queuetuner.h
       src/sound.cpp
       src/sound.h
       src/urlcatcher.cpp
       src/urlcatcher.h
       src/viewer/irccontextmenus.cpp
       src/viewer/irccontextmenus.h
       src/viewer/topicedit.cpp
       src/viewer/topicedit.h
       src/viewer/topichistoryview.cpp
       src/viewer/topichistoryview.h
       src/viewer/viewspringloader.cpp
       src/viewer/viewspringloader.h
Copyright: 2011, Eike Hein
           2010-2012, Eike Hein <hein@kde.org>
           2008, Eli J. MacKenzie <argonel at gmail.com>
           2012, Kristopher Kerwin <kkerwin1@gmail.com>
           2009, Peter Simonsson <peter.simonsson@gmail.com>
           2005-2007, Peter Simonsson <psn@linux.se>
License: GPL-2+3+KDEeV

Files: doc/*
       po/ca/docs/*
       po/de/docs/*
       po/es/docs/*
       po/it/docs/*
       po/nl/docs/*
       po/pt/docs/*
       po/pt_BR/docs/*
       po/sv/docs/*
       po/uk/docs/*
Copyright: 2003-2005, Gary R. Cramblit <garycramblitt@comcast.net>
License: GFDL-NIV-1.2+

Files: po/*.po
Copyright: 2006-2010, Canonical Ltd, and Rosetta Contributors
           2003, Dario Abatianni <eisfuchs@tigress.com>
           2003-2019, Free Software Foundation, Inc.
           2002-2018, the Konversation team
           2006-2020, This_file_is_part_of_KDE
License: GPL-2+

Files: po/uk/konversation.po
Copyright: 2009-2020, This_file_is_part_of_KDE
License: LGPL-2.1+3+KDEeV-translations

Files: src/config/configdialog.cpp
       src/config/configdialog.h
Copyright: 2003, Benjamin C Meyer <ben+kdelibs@meyerhome.net>
           2014, Eike Hein
           2004, Michael Brade <brade@kde.org>
           2003, Waldo Bastian <bastian@kde.org>
License: LGPL-2+

Files: src/guess_ja.cpp
       src/guess_ja.h
Copyright: 2000-2003, Shiro Kawai <shirok@users.sourceforge.net>
License: BSD-3-Clause

Files: src/viewer/osd.cpp
Copyright: 2004, Christian Muehlhaeuser <chris@chris.de>
           2005, Gábor Lehel <illissius@gmail.com>
           2003, Laur Ivan <laurivan@eircom.net>
           2008, Mark Kretschmann <kretschmann@kde.org>
           2004-2005, Max Howell <max.howell@methylblue.com>
           2004-2006, Seb Ruiz <ruiz@kde.org>
License: LGPL-2

Files: debian/*
Copyright: 2007-2020, Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
           2021, Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
           2007-2010, Modestas Vainius <modestas@vainius.eu>
           2003-2007, Nathaniel W. Turner <nate@houseofnate.net>
License: GPL-2+

License: BSD-3-Clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted (subject to the limitations in the
 disclaimer below) provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the
 distribution.
 .
 * The name of the contributors may not be used to endorse or
 promote products derived from this software without specific prior
 written permission.
 .
 NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
 GRANTED BY THIS LICENSE.  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
 HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along
 with this software. If not, see
 <https://creativecommons.org/publicdomain/zero/1.0/>.
 --
 On Debian systems, the complete text of the CC0 Public Domain Dedication
 can be found in `/usr/share/common-licenses/CC0-1.0’.

License: GFDL-NIV-1.2+
 Permission is granted to copy, distribute and/or modify this document under
 the terms of the GNU Free Documentation License, Version 1.2 or any later
 version published by the Free Software Foundation; with no Invariant
 Sections, no Front-Cover Texts, and no Back-Cover Texts.
 --
 On Debian systems, the complete text of the GNU Free Documentation License
 version 1.2 can be found in `/usr/share/common-licenses/GFDL-1.2’.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 --
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2’.

License: GPL-2+3+KDEeV
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of
 the License or (at your option) version 3 or any later version
 accepted by the membership of KDE e.V. (or its successor approved
 by the membership of KDE e.V.), which shall act as a proxy
 defined in Section 14 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public License version 2
 can be found in the file `/usr/share/common-licenses/GPL-2’, likewise, the full
 text of the GNU General Public License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3’.

License: LGPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License version 2 as
 published by the Free Software Foundation
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details
 .
 You should have received a copy of the GNU Library General Public
 License along with this program; if not, write to the
 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 --
 On Debian systems, the complete text of the GNU Library General Public
 License version 2 can be found in `/usr/share/common-licenses/LGPL-2’.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
 --
 On Debian systems, the full text of the GNU Library General Public License
 version 2 can be found in the file `/usr/share/common-licenses/LGPL-2’.

License: LGPL-2.1+3+KDEeV-translations
 This file is distributed under the license LGPL version 2.1 or
 version 3 or later versions approved by the membership of KDE e.V.
 --
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1’,
 likewise, the complete text of the GNU Lesser General Public License version
 3 can be found in `/usr/share/common-licenses/LGPL-3’.
